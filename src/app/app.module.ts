import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AddSurveyComponent } from './add-survey/add-survey.component';
import { SurveysComponent } from './surveys/surveys.component';

import {Route, RouterModule} from '@angular/router';

import { ServiceBDService } from './service-bd.service';

const routes: Route[] = [
  {path: '', component: LoginComponent},
  {path: 'addSurvey', component: AddSurveyComponent},
  {path: 'surveys', component: SurveysComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AddSurveyComponent,
    SurveysComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
  ],
  providers: [ServiceBDService],
  bootstrap: [AppComponent]
})
export class AppModule { }
