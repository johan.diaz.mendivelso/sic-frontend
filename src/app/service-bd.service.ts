import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from './models/user';

@Injectable({
  providedIn: 'root'
})
export class ServiceBDService {

  constructor(private httpClient: HttpClient) { }

  login() {
    return this.httpClient.get<User>('http://localhost:8080/Encuesta/user/login');
  }
}
