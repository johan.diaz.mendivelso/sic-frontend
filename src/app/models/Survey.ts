export interface User {
  'noDocument': number;
  'user': {
    'userID': number;
    'name': number;
    'user': string;
    'password': string;
  };
  'BrandPc': {
    'brandPcId': number;
    'description': number;
  };
  'email': string;
  'comment': string;
}
