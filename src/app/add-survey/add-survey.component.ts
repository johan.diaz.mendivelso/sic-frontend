import { Component, OnInit, Input } from '@angular/core';
import {User} from '../models/user';
import {BrandPc} from '../models/BrandPc';
import {UrlSerializer} from '@angular/router';

@Component({
  selector: 'app-add-survey',
  templateUrl: './add-survey.component.html',
  styleUrls: ['./add-survey.component.css']
})
export class AddSurveyComponent implements OnInit {
  @Input() noDocument: number;
  @Input() user: {};
  @Input() email: string;
  @Input() comment: string;
  @Input() brandPc: string;

  constructor() {
    this.noDocument = 0;
    this.user = {};
    this.brandPc = '';
    this.email = '';
    this.comment = '';
  }

  addSurvey(noDocument: any, email: any, comment: any, brandPc: any): void{
    console.log(noDocument, email, comment, brandPc);
  }

  ngOnInit(): void {
  }

}
